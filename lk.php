<?php
session_start();
error_reporting(E_ALL);

class lk {

	private $config;
	private $_id;
	private $db;
	private $userdata;

	function __construct() {

		$this->config();

		$this->setup();
		$this->out();
	}

	function config() {
		if ( ! $this->config) {
			$defaults = array(
				"DB_HOST"     => "localhost",
				"DB_NAME"     => "bd",
				"DB_USER"     => "root",
				"DB_PASSWORD" => "",
				"table"       => "tmp_test_users",
			);
			if (file_exists('.env')) {
				$this->config = array_merge($defaults, parse_ini_file('.env', false));
			} else {
				return $defaults;
			}
		}

		return $this->config;
	}

	function setup() {
		$this->db = new mysqli(
			$this->config['DB_HOST'],
			$this->config['DB_USER'],
			$this->config['DB_PASSWORD'],
			$this->config['DB_NAME']
		);

		$check_table = $this->db->query("SHOW TABLES LIKE '" . $this->config["table"] . "'");

		if ( ! $check_table or ! $check_table->num_rows) {
			if ( ! $this->db->query("CREATE TABLE `" . $this->config["table"] . "` (
 `id` int(11) unsigned NOT NULL auto_increment,
 `login` varchar(50) NOT NULL,
 `password` varchar(50) NOT NULL,
 `name` varchar(300) NOT NULL,
 `email` varchar(100) NOT NULL,
 `hash` varchar(32) NOT NULL default '',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci")) {
				die("Error create table");
			}
		}
	}

	function out() {
		switch ( ! empty($_GET["action"]) ? $_GET["action"] : false ) {
			case "logout":
				$this->clearAuth();
				header("Location: lk.php?action=Goodbye");
				die();
			case "register":
				if ($this->id()) {
					header("Location: lk.php?action=settings");
					die();
				}
				$attr = array(
					'login' => false,
					'email' => false,
					'name'  => false,
				);
				if (isset($_POST['save'])) {
					foreach ( $attr as $k => $v ) {
						if (isset($_POST[ $k ])) {
							$attr[ $k ] = $_POST[ $k ];
						}
					}
					if ( ! ($attr['messages'] = $this->register())) {
						header("Location: lk.php?action=login&from=register");
						die();
					}
				}
				$this->view("header", array( "title" => "Регистрация" ));
				$this->view("register", $attr);
				break;
			case "settings":
				if ( ! $this->id()) {
					header("Location: lk.php?action=login&from=settings");
					die();
				}
				$attr = array(
					'login' => $this->getUserData(false, 'login'),
					'email' => $this->getUserData(false, 'email'),
					'name'  => $this->getUserData(false, 'name'),
				);
				if (isset($_POST['save'])) {
					if (isset($_POST['name'])) {
						$attr["name"] = $_POST['name'];
					}
					$attr['messages'] = $this->edit();
				}
				$this->view("header", array(
					"title"     => "Настройки",
					"pagetitle" => sprintf("Ваши настройки, %s", $this->getUserData(false, "name"))
				));
				$this->view("settings", $attr);
				break;
			case "check":
				if ($this->id()) {
					header("Location: lk.php?action=dashboard");
					die();
				} else {
					$this->view("header", array( "title" => "Проверка авторизации" ));
					$this->view("login", array( "messages" => "Ошибка авторизации" ));
				}
				break;
			case "login":
				if ($this->id()) {
					header("Location: lk.php?action=dashboard&from=login");
					die();
				}

				$messages = '';
				if (isset($_POST['login'])) {
					if ($this->login()) {
						header("Location: ?action=check");
						exit();
					} else {
						$messages = "Авторизация неудачна. проверьте логин и пароль.";
					}
				}
				$this->view("header", array( "title" => "Авторизация" ));
				$this->view("login", array( "messages" => $messages ));
				break;
			case "dashboard":
			default:
				$this->view("header", array(
					"title"     => "DashBoard",
					"pagetitle" => $this->id() ? sprintf("Добро пожаловать, %s", $this->getUserData(false, "name") ) : "Авторизуйтесь для доступа в кабинет"
				));
				$this->view("dashboard");
		}

		$this->view("footer");
	}

	function getUserData($id = false, $field = false) {
		$id = $id ? $id : $this->id();
		if (empty($this->userdata) and $id) {
			$query          = $this->db->query("SELECT * FROM " . $this->config["table"] . " WHERE id = '" . intval($id) . "' LIMIT 1");
			$this->userdata = $query->fetch_row();
		}
		if ($field) {
			return isset($this->userdata[ $field ]) ? $this->userdata[ $field ] : null;
		}

		return $this->userdata;
	}

	function clearAuth() {
		// unset($_SESSION["auth"]);
		session_destroy();
	}

	function setAuth($auth) {
		$_SESSION["auth"] = $auth;
	}

	function id() {
		$this->_id = false;

		if (isset($_SESSION["auth"])) {
			$query    = $this->db->query("SELECT id,login,name,email,hash FROM " . $this->config["table"] . " WHERE hash = '" . $_SESSION["auth"] . "' LIMIT 1");
			$userdata = $query->fetch_assoc();

			if (empty($userdata)) {
				$this->clearAuth();
			} else {
				$this->_id      = $userdata["id"];
				$this->userdata = $userdata;
			}
		}

		return $this->_id;
	}

	function login() {
		if (empty($_POST['login']) or empty($_POST['password'])) {
			return false;
		}
		$login = $this->db->real_escape_string($_POST['login']);

		$r    = $this->db->query("SELECT id, password FROM " . $this->config["table"] . " WHERE login='" . $login . "' limit 1");
		$data = $r->fetch_assoc();

		if ( ! empty($data['password']) and $data['password'] === md5(md5(trim($_POST['password'])))) {
			$data["hash"] = md5(substr(md5(mt_rand()), 0, 10));

			$r = $this->db->prepare("UPDATE " . $this->config["table"] . " SET hash=? WHERE id=?");
			$r->bind_param("ss", $data["hash"], $data["id"]);
			$r->execute();
			if ($r->affected_rows) {
				$this->setAuth($data["hash"]);

				return true;
			}
		}

		return false;
	}

	function register() {

		$errors = [];
		if ( ! preg_match("/^[a-zA-Z0-9]+$/", $_POST['login'])) {
			$errors[] = "Логин может состоять только из букв английского алфавита и цифр";
		}

		// if ( empty($_POST['name'])) {
		// 	$errors[] = "Необходимо указать ФИО";
		// }

		$query = $this->db->query("SELECT id FROM " . $this->config["table"] . " WHERE login='" . $this->db->real_escape_string($_POST['login']) . "'");
		if ($query and $query->num_rows > 0) {
			$errors[] = "Пользователь с таким логином уже существует в базе данных";
		}

		if (empty($_POST['password']) or empty($_POST['password_confirm']) or
		    trim($_POST['password']) !== trim($_POST['password_confirm'])
		) {
			$errors[] = "Пароль не указан или неверно подтвержден";
		}

		if (count($errors) == 0) {
			$login = $this->db->real_escape_string($_POST['login']);
			$name  = $this->db->real_escape_string($_POST['name']);
			$email = $this->db->real_escape_string($_POST['email']);

			// двойное хеширование как вариант усложнения
			$password = md5(md5(trim($_POST['password'])));

			$r = $this->db->prepare(
				"INSERT INTO  " . $this->config["table"] .
				" SET `login` = ?, `name` = ?, `email` = ?, `password` = ? ");
			$r->bind_param('ssss', $login, $name, $email, $password);
			$r->execute();
			if ($r->affected_rows) {

				return false;

			} else {
				$errors[] = 'Database Error';
			}
		}

		return $errors;
	}

	function edit() {

		$mess = [];

		if ( ! empty($_POST['password']) and
		     (empty($_POST['password_confirm'])
		      or trim($_POST['password']) !== trim($_POST['password_confirm']))) {
			$mess[] = "Пароль не указан или неверно подтвержден";
		}

		if (count($mess) == 0) {
			$name     = ! empty($_POST['name']) ? $this->db->real_escape_string($_POST['name']) : "";
			$password = '';
			if ( ! empty($_POST['password'])) {
				$password = md5(md5(trim($_POST['password'])));
			}

			$r  = $this->db->prepare(
				"UPDATE " . $this->config["table"] .
				" SET `name` = ? " . ($password ? ", `password` = ?" : '') .
				" WHERE `id` = ?");
			$id = $this->id();
			if ($password) {
				$r->bind_param('ssi', $name, $password, $id);
			} else {
				$r->bind_param('si', $name, $id);
			}
			$r->execute();
			if ( ! $r->affected_rows) {
				$mess[] = "Без изменений";
			} else {
				$mess[] = "Успешно обновлено";
			}
		}

		return $mess;
	}

	function view($_tpl_name = '', $_atts = array()) {
		if ($_tpl_name and file_exists("view/$_tpl_name.php")) {
			$_atts = array_merge(array( "id" => $this->id() ), $_atts);

			extract($_atts);
			include "view/$_tpl_name.php";

			if ($_tpl_name != "messages" and ! empty($messages)) {
				$this->view("messages", array( "messages" => $messages ));
			}

			return true;
		}

		return false;
	}
}

new lk;

