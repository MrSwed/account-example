<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo ! empty($title) ? $title : "Test auth page"; ?></title>

</head>
<body>
<header>
	<a class="Logo" href="?">Home</a>
	<?php if ( ! empty($pagetitle) or ( ! empty($title) and ($pagetitle = $title))) { ?>
		<h1><?php echo $pagetitle; ?></h1>
	<?php } ?>

</header>