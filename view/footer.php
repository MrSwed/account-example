<footer>
	<ul class="menu">
		<?php if ( ! empty($id)) { ?>
			<li><a href="?action=dashboard">Личный кабинет</a></li>
			<li><a href="?action=settings">Настройки</a></li>
			<li><a href="?action=logout">Выйти</a></li>
		<?php } else { ?>
			<li><a href="?action=login">Вход</a></li>
			<li><a href="?action=register">Регистрация</a></li>
		<?php } ?>
	</ul>
</footer>
</body>
</html>
